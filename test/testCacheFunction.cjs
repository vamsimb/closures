const cacheFunction = require('../cacheFunction.cjs')

cb = (elements) => elements.map(element => element * 2)

const result = cacheFunction(cb)
console.log(result(1, 2, 1))