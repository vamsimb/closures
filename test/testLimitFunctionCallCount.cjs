const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

cb = (elements) => elements.map(element => element + 1)

const result = limitFunctionCallCount(cb, 5)

result(1, 2, 3)