function cacheFunction(cb) {
	let cache = {}
	if (typeof (cb) === 'function') {
		function call(...args) {

			for (let key of args) {

				if (cache[key] === undefined) cache[key] = cb(args)

				else return cache
			}
			return cache
		}
		return call
	}
	return {};
}

module.exports = cacheFunction;
