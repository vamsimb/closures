function limitFunctionCallCount(cb, callCount) {
  if (typeof (cb) === 'function') {
    let count = 0
    function call(...args) {
      while (count < callCount) {
        console.log(cb(args))
        count += 1
      }
    }
    return call
  }
  return null;
}



module.exports = limitFunctionCallCount;